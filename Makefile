ASDF_INSTALLED = command -v asdf > /dev/null 2>&1
TOOLS := yq jq glab shellcheck ruby
CONFIG_FILE = "config.yaml"

.PHONY: all clean

all: clean

clean:
	@rm -rf ./.build
	@mkdir -p ./.build/assets

verify-asdf-installed:
	@if !$(ASDF_INSTALLED); then \
		echo "asdf is not installed. Please install asdf first: https://asdf-vm.com/guide/getting-started.html"; \
		exit 1; \
	fi

install-prerequisites: verify-asdf-installed
	@for tool in $(TOOLS); do \
		if ! asdf list $$tool &> /dev/null; then \
            asdf plugin add $$tool; \
        fi \
	done
	asdf install; \

validate-config:
	@./scripts/lint/validate_config.sh $(CONFIG_FILE)

disallow-job-id:
	@./scripts/lint/disallow_job_id.sh $(CONFIG_FILE)

fetch-assets: clean
	@./scripts/build/fetch_assets.sh $(OS) $(PLATFORM) $(CONFIG_FILE)

prepare-docker-build: fetch-assets
	@./scripts/build/prepare_docker_build.sh $(OS) $(PLATFORM)

docker-build-and-publish: prepare-docker-build
	@./scripts/build/docker_build_and_publish.sh $(OS) $(PLATFORM) $(ENV) $(IMAGE_NAME)
