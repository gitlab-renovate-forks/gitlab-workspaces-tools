# gitlab-workspaces-tools

This repository contains code which is responsible for injecting tools inside a workspace on startup. The currently supported tools are listed in the [`config.yaml`](config.yaml) file.

## Config

The [`config.yaml`](config.yaml) file specifies the dependencies needed to build the image. Its schema is as follows:

``` yaml
assets: ## assets describes an array of dependencies, each with the following structure:
  - id: "{{ Identifies an asset. Used to define asset url builder function in scripts }}"
    repository: "{{ Git URL of the repository. Must be double-quoted }}"
    tag: "{{ Git tag of the release. Must be double quoted }}"
    file_name: "{{ Regex of the file to downloaded from the release assets }}"
    job_id: "{{ Optional, allows obtaining an asset from a CI job to publish development builds }}"
```

The `RENOVATE_BOT_REGEX` in the [`config validation script`](scripts/lint/validate_config.sh) validates the config so we do not break [gitlab-renovate-bot](https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/-/blob/main/renovate/gitlab-workspace-tools/gitlab-workspace-tools.config.js?ref_type=heads) dependency update suggestions. If a regex update is required, we must also update the gitlab-renovate-bot config regex and vice versa.

### Automation

Each asset in `config.yaml` is monitored by the [gitlab-renovate-bot](https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/-/blob/main/renovate/gitlab-workspace-tools/gitlab-workspace-tools.config.js?ref_type=heads). The bot opens MRs when a new tag is observed
for a dependency in the `config.yaml`. This notifies the team of any updates and gives them a chance to verify the builds with new dependencies before approving and merging the MR.

### Adding a new asset

- Add the asset in [`config.yaml`](config.yaml) following the structure of existing assets defiend.
- Define two functions in [`fetch_assets.sh`](/scripts/build/fetch_assets.sh). Assuming `id` is the `id` of the asset in the config file -
  - Function `build_url_for_${id}` prints the URL to fetch the asset from.
  - Function `get_asset_dest_name_for_${id}` prints the destination name of the file to download the asset to. This truncates the version name in the file, changes the extension of the file if required.

## Pipeline setup

- When a merge request is opened, the assets listed in the `config.yaml` are fetched and used to prepare a build and publish an image with tag `dev-${timestamp}`. This image can be used for test verification purposes. DO NOT use this image for production purposes since they will be deleted.
- When new changes are merged to master, the assets listed in the `config.yaml` are fetched and used to prepare a build and publish an image with tag `dev-${timestamp}`. This image can be used for test verification purposes. DO NOT use this image for production purposes since they will be deleted.
- When a new tag is created, the assets listed in the `config.yaml` are fetched and used to prepare a build and publish an image with the new tag. This image can be used for production purposes.
- Images are pushed to the container registry of the project. We avoid hardcoding the container registry name to support the same workflow on forked repositories.
- [TODO](https://gitlab.com/gitlab-org/remote-development/gitlab-workspaces-tools/-/issues/2): Cron job which is run weekly and deletes all images with `dev-${timestamp}` tags.

## E2E manual testing

There is not any automated E2E test coverage for releasing a new version and testing it in Workspaces.
Ensure the following checks are done manually before merging an MR or making a new release.

- Create a workspace with the updated image.
- Check if the terminal is working correctly by running some commands like `ls, uname, id`.
- Check if the extensions marketplace is working correctly by installing/removing some extensions from the marketplace.
- Check if the `git` functionality is working correctly by making a commit through the UI and pushing it upstream.
- Check if the GitLab Workflow extension is working correctly by verifying if issue/MR list is visible, code suggestions is working, etc.
- Check the pod logs and extensions logs(where applicalbe) for every action to see if there is something unusual.

## Release process

Before creating a release, ensure that [E2E manual testing](#e2e-manual-testing) is performed.

To trigger the production build, a new tag must be manually pushed. The tag
should follow [semantic versioning](https://semver.org/#summary) rules. For example:

``` shell
TAG_NAME=0.1.0
# create an annotated tag
git tag -a ${TAG_NAME}
git push origin ${TAG_NAME}
```

This should then be followed up by making a release manually from the pushed tag. TODO: [automate release process](gitlab-org/remote-development/gitlab-workspaces-tools#3)

## Development builds

Development builds are published in this project's 
[container registry](https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-tools/container_registry/). 
Adding a `job_id` attribute to an asset entry in `config.yaml` allows fetching an asset from a CI job's artifact. The
goal of this feature is generating development builds for testing purposes. 
_This feature is only available in Merge Request pipelines_. Merge Requests' pipelines have a `block_assets_with_job_id`
job that will fail if it finds `job_id` attribute in an asset definition. This is a deliberate decision to 
avoid merging `config.yaml` files that contain a `job_id` to the `main` branch.

When using `job_id`, the `file_name` attribute's value should satisfy the following properties:

1. The contains artifact's full path.
1. It uses regexp placeholders to allow downloading builds for different operating systems and CPU architectures. 

For example, this is an asset declaration for the GitLab VSCode Fork:

```yaml
assets:
  - id: "gitlab_web_ide_vscode_fork"
    repository: "https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork"
    file_name: ".build/vscode-reh-web/vscode-reh-web-.*[-].*[-]1.89.1-1.0.0-dev-20240725061844.tar.gz"
    job_id: "7424314201"
```

## Local development

### Install dependencies

- Install [make](https://www.gnu.org/software/make/)
- Install [asdf](https://asdf-vm.com/guide/getting-started.html) 
- `make install-prerequisites` (or `mise install` if you use mise)
- `bundle install`
- `lefthook install`

### Run Lefthook manually

To run linters manually: `lefthook run pre-commit`

_This is only needed to run Lefthook manually: ie if checking if it will pass without actually trying to commit, or if it is not set to run automatically,_


### Docker build

```shell
OS=linux PLATFORM=amd64 ENV=local make docker-build-and-publish
```

This will yield a local image of with a tag of `local-${timestamp}`

### Troubleshooting

Individual steps in the build process can be run locally by triggering their make target. For example:

```shell
OS=linux PLATFORM=amd64 make fetch-assets
```

This could be benefical when debugging locally where you might need to asses or modify the output of each job.
