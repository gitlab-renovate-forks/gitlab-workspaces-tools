#!/usr/bin/env bash

# This script ensures that assets defined in config.yaml file doesn't contain a `job_id` attribute. This
# script is only called in the disallow_job_id CI job to prevent merging Merge Requests that contain 
# a config.yaml file that violates this rule.
#
# It uses the following arguments:
# $CONFIG_FILE - config file path e.g. config.yaml.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

CONFIG_FILE=$1

if [[ $(yq eval '.assets[] | has("job_id")' "$CONFIG_FILE" | grep "true" | uniq) == "true" ]]; then
    echo "Do not merge a config.yaml file with a job_id attribute. This helps avoid publishing production builds for development artifacts";
    exit 1;
fi