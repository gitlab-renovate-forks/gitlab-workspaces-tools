#!/usr/bin/env bash

# This script ensures the config is valid yaml and it does not break renovate-bot dependency tracking.
#
# It uses the following arguments:
# $CONFIG_FILE - config file path e.g. config.yaml.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

CONFIG_FILE=$1
# This regex is the same one used by the renovate-bot to parse the config, see `matchStrings` config in:
# https://gitlab.com/gitlab-org/frontend/renovate-gitlab-bot/-/blob/main/renovate/gitlab-workspace-tools/gitlab-workspace-tools.config.js?ref_type=heads
# We are performing this check on each asset so as not to break renovate-bot dependency update tracking
RENOVATE_BOT_REGEX='repository: "https:\/\/gitlab\.com\/(?<packageName>.*)"\s+tag: "(?<currentValue>[-\w.]+)"'

yq eval '.' "$CONFIG_FILE"
assets_count=$(yq eval '.assets | length - 1' "${CONFIG_FILE}")

for index in $(seq 0 "$assets_count"); do
    asset=$(yq eval ".assets[$index]" "${CONFIG_FILE}")
    has_job_id=$(yq eval ".assets[$index] | has(\"job_id\")" "${CONFIG_FILE}")
    has_tag=$(yq eval ".assets[$index] | has(\"tag\")" "${CONFIG_FILE}")

    if [[ "$has_job_id" == "true" && "$has_tag" == "true" ]]; then
        echo "Don't set a job_id and a tag for the same asset. This helps avoid publishing production builds for development artifacts."
        echo "Invalid asset ${asset}"
        exit 1
    fi

    if [[ "$has_job_id" == "true" && "${CI_MERGE_REQUEST_IID}" != "" ]]; then
        echo "Asset with a job_id detected for a merge request pipeline. Skipping renovate regex validation."
        continue
    fi

    # Our RENOVATE_BOT_REGEX is a Perl-compatible regular expressions (PCREs). Grep does not support PCREs, ggrep (GNU grep) does,
    # but the option is experimental when combined with the -z (--null-data) option and also requires us to installl a new grep util locally.
    # We have decided to use the Perl utility for regex matching.
    # Option -0777 sets Perl's input record separator to 0777, which means it reads the entire input at once (slurp mode).
    # The if condition checks the content matches the regex pattern (/.../s enables dot to match newline). 
    # If the pattern matches, it exits with status 0. If the pattern does not match, it exits with status 1 and the offending asset is reported.
    if ! echo "$asset" |  perl -0777 -ne 'if (/'"$RENOVATE_BOT_REGEX"'/s) { exit 0; } else { exit 1; }' ; then
        echo "The following asset violates renovate-bot regex"
        echo "$asset"
        exit 1;
    fi
done
