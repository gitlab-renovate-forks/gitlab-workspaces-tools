#!/usr/bin/env bash

# This script build and publishes the docker image
#
# It uses the following arguments
# $OS - OS e.g. linux.
# $PLATFORM - Platform e.g. amd64.
# $IMAGE_NAME - docker image name e.g "registry.gitlab.com/gitlab-org/workspace/gitlab-workspaces-tools".
# $ENV - environment e.g. local.

# TODO: Look into DRYing up this boilerplate: https://gitlab.com/gitlab-org/gitlab-web-ide-vscode-fork/-/issues/7
# See https://www.gnu.org/software/bash/manual/html_node/The-Set-Builtin.html
set -o errexit  # AKA -e - exit immediately on errors (http://mywiki.wooledge.org/BashFAQ/105)
set -o xtrace   # AKA -x - get bash "stacktraces" and see where this script failed
set -o pipefail # fail when pipelines contain an error (see http://www.gnu.org/software/bash/manual/html_node/Pipelines.html)

OS=$1
PLATFORM=$2
ENV=$3
IMAGE_NAME=$4

if [ -z "${OS}" ]; then
    echo "\$OS is not set"
    exit 1
fi

if [ -z "${PLATFORM}" ]; then
    echo "\$PLATFORM is not set"
    exit 1
fi

if [ -z "${ENV}" ]; then
    echo "\$ENV is not set"
    exit 1
fi

if [ -z "${IMAGE_NAME}" ]; then
    echo "\$IMAGE_NAME is not set. Using default 'registry.gitlab.com/gitlab-org/workspaces/gitlab-workspaces-tools'"
    IMAGE_NAME="registry.gitlab.com/gitlab-org/workspaces/gitlab-workspaces-tools"
fi

SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
ROOT_DIR="$(cd "$SCRIPT_DIR/../.." && pwd)"
TOOLS_INJECTOR_ARTIFACTS_DIR="${ROOT_DIR}/.build/tools-injector"
mkdir -p "${TOOLS_INJECTOR_ARTIFACTS_DIR}"

echo "Building docker image for ${OS} ${PLATFORM}"
IMAGE_NAME_WIHTOUT_PREFIX=$(basename "$IMAGE_NAME")
docker build --platform "${OS}/${PLATFORM}" -t "${IMAGE_NAME}:latest" -f Dockerfile .

case $ENV in
    "prod")
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        docker tag "${IMAGE_NAME}:latest" "${IMAGE_NAME}:${CI_COMMIT_TAG}"
        docker push "${IMAGE_NAME}:${CI_COMMIT_TAG}"
        ;;
    "dev")
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        hash=$(TZ=UTC date '+%Y%m%d%H%M%S')
        docker tag "${IMAGE_NAME}:latest" "${IMAGE_NAME}:dev-${hash}"
        docker push "${IMAGE_NAME}:dev-${hash}"
        ;;
    "local")
        hash=$(TZ=UTC date '+%Y%m%d%H%M%S')
        DEST="${TOOLS_INJECTOR_ARTIFACTS_DIR}/${IMAGE_NAME_WIHTOUT_PREFIX}:local-${hash}.tar.gz"
        docker tag "${IMAGE_NAME}:latest" "${IMAGE_NAME}:local-${hash}"
        docker save "${IMAGE_NAME}:local-${hash}" | gzip > "${DEST}"
        ;;
    *)
        echo "Unknown environment $ENV"
        exit 1
        ;;
esac
