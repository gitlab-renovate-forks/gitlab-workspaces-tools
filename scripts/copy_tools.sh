#!/bin/sh

# This script copies the tools to be injected into the workspace on startup in init container.
#
# It uses the following environment variables
# $GL_TOOLS_DIR - directory where the tools are copied. This directory contains files used by the "init_tools.sh" script.

mkdir -p "${GL_TOOLS_DIR}"
cp -R vscode-reh-web "${GL_TOOLS_DIR}"/
cp init_tools.sh "${GL_TOOLS_DIR}"/
